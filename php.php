<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestform PHP</title>
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <h1 style="text-align: center;margin:15px">Test Gestform PHP</h1>
    <div class="mybox">
        <form class="mybox" action="./phpresponse.php" method="post">

            <label for="size">Taille de la liste : </label>
            <input type="number" name="size" id="size">

            <label for="min">Nombre minimum : </label>
            <input type="number" name="min" id="min">

            <label for="max">Nombre maximum : </label>
            <input type="number" name="max" id="max">

            <input id="reloadLink" type="submit" value="C'est parti">

        </form>
    </div>

</body>

</html>