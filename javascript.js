/**
 * Return a integer beetween min and max
 * 
 * @param {Number} max 
 * @param {Number} min 
 * @returns {Number} 
 */
function getRandomNumber(min , max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

/**
 * For each numbers of a list , return an answer according to the number
 * 
 * @param {Number} size size of the list
 * @param {Number} min minimum number chosen
 * @param {Number} max maximum number chosen
 */
function gestform(size, min, max) {

    // Récupération de la liste HTML pour l'affichage
    var htmlList = document.querySelector("#responseList");
    var numbList = [];

    // Création de la liste aléatoire
    for (i = 0; i < size; i++){
        numbList.push(getRandomNumber(min , max));
    }

    // Pour chaque nombre de la liste, 
    // on vérifie les conditions prédéfinies
    // puis on stocke la réponse reçue
    numbList.forEach(num => {
        var response;
        if ((num % 3 === 0 ) && (num % 5 === 0 )) {   
            response = num + " : Gestform";
        }
        else if(num % 3 === 0 ){
            response = num + " : Geste";
        }
        else if(num % 5 === 0 ){
            response = num + " : Forme";
        }
        else {
            response = num;
        }

        // Affichage sur la page HTML
        var li = document.createElement('li');
        li.setAttribute('class', 'responseItem')
        htmlList.appendChild(li);
        li.innerHTML = response ;
    });
}

document.addEventListener("DOMContentLoaded", function () { 
    var userSize = parseInt(prompt("Taille de la liste : ", "10"));
    var userMin = parseInt(prompt("Nombre minimum : ", "-1000"));
    var userMax = parseInt(prompt("Nombre maximum : ", "1000"));

    gestform(userSize, userMin, userMax);   
});
