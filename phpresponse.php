<?php
/**
 * Return a random integer beetween $min and $max
 * @param Integer min minimum number chosen
 * @param Integer max maximum number chosen
 */
function getRandomNumber($min, $max)
{
    return rand($min, $max);
}
/**
 * Return a list of randoms integers beetween $min and $max
 * @param Integer size size of the list
 * @param Integer min minimum number chosen
 * @param Integer max maximum number chosen
 */
function getNumberList($size, $min, $max)
{
    $numberList = [];
    for ($i = 0; $i < $size; $i++) {
        array_push($numberList, getRandomNumber($min, $max));
    };

    return $numberList;
}

/**
 * For each numbers of a list , return an answer according to the number
 * @param Integer size size of the list
 * @param Integer min minimum number chosen
 * @param Integer max maximum number chosen
 */
function gestform($size, $min, $max)
{

    // Création de la liste de nombres aléatoires
    $numbList = getNumberList($size, $min, $max);
    $answers = [];

    // Pour chaque nombre de la liste, 
    // on vérifie les conditions prédéfinies
    // puis on stocke la réponse reçue
    foreach ($numbList as $num) {
        if ($num % 3 === 0 && $num % 5 === 0) {
            $resp = $num . " : Gestform !";
        } elseif ($num % 3 === 0) {
            $resp = $num . " : Geste ";
        } elseif ($num % 5 === 0) {
            $resp = $num . " : Forme ";
        } else {
            $resp = $num;
        }
        array_push($answers, $resp);
    }
    // On retourne la liste des réponses
    return $answers;
}

// Vérification de sécurité
// on vérifie que les valeurs size , min et max sont définies dans $_POST 
if (isset($_POST['size'])) {
    $userSize = intval(($_POST['size']));
} else {
    header('Location: ./php.php ');
    exit;
}
if (isset($_POST['min'])) {
    $userMin = intval(($_POST['min']));
} else {
    header('Location: ./php.php ');
    exit;
}
if (isset($_POST['max'])) {
    $userMax = intval(($_POST['max']));
} else {
    header('Location: ./php.php ');
    exit;
}

// On lance la fonction gestform() avec les données reçues de l'utilisateur
// et on stocke les réponses pour les afficher plus bas
$userAnswers = gestform($userSize, $userMin, $userMax);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultats - Gestform PHP</title>
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <h1 style="text-align: center;margin:15px">Résultats - Test Gestform PHP</h1>

    <div class="mybox">
        <ul id="responseList">
            <?php
            // On affiche chaque réponse sur la page HTML
            foreach ($userAnswers as $answer) {
                echo '<li class="responseItem" >' . $answer . '</li>';
            }
            ?>

        </ul>

        <a id="reloadLink" href="./php.php">Recharger</a>
        <a id="reloadLink" href="./index.php">Accueil</a>

    </div>
</body>

</html>