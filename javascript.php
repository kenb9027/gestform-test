<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestform Javascript</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <h1 style="text-align: center;margin:15px">Test Gestform Javascript</h1>
    <div class="mybox">
        <ul id="responseList"></ul>
        <a id="reloadLink" href="">Recharger la page</a>
        <a id="reloadLink" href="./index.php">Accueil</a>
    </div>

    <script src="javascript.js"></script>
</body>

</html>